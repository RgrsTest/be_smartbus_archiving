<?php

namespace SmartbusSeeder;

use Illuminate\Database\Seeder;
use Symfony\Component\Finder\Finder;
use Illuminate\Support\Facades\Log;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      \Klay\Models\Usuario::firstOrCreate([
        'correo' => 'desarrollo@stx.com.mx'],[
        'contrasena' => bcrypt('0000'),
        'apikey' => nanoId(),
        'roles' => [],
        'credenciales' => []
      ]);

      foreach ((new Finder)->in(__DIR__)->files()->name('*_*.json') as $json) {

        [$type, $name] = explode('_', basename($json->getRelativePathname(), '.json'), 2);

        Log::channel("Smartbus")->info($name);

        \Archiving\Models\Scheme::updateOrcreate([
          'name' => $name,
          'type' => $type ],[
          'schema' => json_decode($json->getContents()),
        ]);
      }

    }
  }
